package com.br.padraoProjeto.strategy;

public class MallardDuck extends Duck {

    public MallardDuck(){
        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
        swimmingBehavior = new swimming();
    }
    public void display(){
        System.out.println("I'm a real Mallard duck");
    }
}

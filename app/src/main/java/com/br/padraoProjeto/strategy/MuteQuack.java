package com.br.padraoProjeto.strategy;

public class MuteQuack implements QuackBehavior {
    public void quack() {
        System.out.println(" <<<Silence>>>");
    }

}

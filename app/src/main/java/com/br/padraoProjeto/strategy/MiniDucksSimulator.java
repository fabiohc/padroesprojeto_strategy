package com.br.padraoProjeto.strategy;

public class MiniDucksSimulator {

    public static void main(String[] args) {
        Duck mallard = new MallardDuck();
        mallard.performQuack();
        mallard.performFly();

        System.out.println("----Atividade 1----");
        mallard.performSwimming();

        System.out.println("----Atividade 2----");
        Duck model = new ModelDuck();
        model.performFly();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();

        System.out.println("----Atividade 3----");
        Duck modelSwDuck = new ModelDuck();
        modelSwDuck.performWalking();
        modelSwDuck.performSwimming();
        modelSwDuck.setWalkingBehavior(new WalkingFast());
        modelSwDuck.setSwimmingBehavior(new SwimmingFast());
        modelSwDuck.performWalking();
        modelSwDuck.performSwimming();

    }

    }

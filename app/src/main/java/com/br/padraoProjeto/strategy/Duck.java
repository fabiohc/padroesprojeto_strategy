package com.br.padraoProjeto.strategy;

public abstract class Duck {

    FlyBehavior flyBehavior;
    QuackBehavior quackBehavior;
    SwimmingBehavior swimmingBehavior;
    WalkingBehavior walkingBehavior;



    public Duck() {
           }

    public abstract void display();

    public void performFly() {
        flyBehavior.fly(); }

    public void performQuack() {
        quackBehavior.quack();
    }

    public void performSwimming (){
        swimmingBehavior.swimming();
    };
    public void performWalking (){
        walkingBehavior.walking();
    };


    public void setFlyBehavior(FlyBehavior fb){
        flyBehavior = fb;
    }
    public void setQuackBehavior(QuackBehavior qb){
        quackBehavior = qb;
    }
    public void setSwimmingBehavior(SwimmingBehavior sb){
        swimmingBehavior = sb;
    }
    public void setWalkingBehavior(WalkingBehavior wb){
        walkingBehavior = wb;
    }



public void swin() {
        System.out.println("All ducks float, even decoys!");
    }


}

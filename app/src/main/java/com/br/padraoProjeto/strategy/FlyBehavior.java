package com.br.padraoProjeto.strategy;

public interface FlyBehavior {
    public void fly();
}

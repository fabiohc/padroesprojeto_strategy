package com.br.padraoProjeto.strategy;

public interface QuackBehavior {
    public void quack();
}
